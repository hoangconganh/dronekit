'''
Created on Oct 10, 2016

'''
from dronekit.test import sitl
import time
def connect():
    print "Start simulator SITL"
    import dronekit_sitl
    sitl = dronekit_sitl.start_default()
    connection_string = sitl.connection_string()
    #Import Drone kit Python
    from dronekit import connect, VehicleMode
    #Connect to the Vehicle
    print("Connecting to the vehicle on: %s" %(connection_string))
    vehicle = connect(connection_string,wait_ready= True)
    
    return vehicle
def getStatus(vehicle):
    #Get some vehicle attributes (state)
    print "Get some vehicle attribute values:"
    print "GPS : %s" %vehicle.gps_0()
    print "Battery: %s" %vehicle.battery
    print "Last heartbeat: %s" %vehicle.last_heartbeat
    print "Is Armable?: %s" %vehicle.is_armable
    print "System status: %s" %vehicle.system_status.state
    print "Mode: %s" % vehicle.mode.name #settable
